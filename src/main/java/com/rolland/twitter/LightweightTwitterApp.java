package com.rolland.twitter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LightweightTwitterApp {

    public static void main(String[] args) {
        SpringApplication.run(LightweightTwitterApp.class, args);
    }
}
