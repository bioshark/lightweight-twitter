package com.rolland.twitter.controller;


import com.rolland.twitter.controller.model.TwitterMessage;
import com.rolland.twitter.controller.model.TwitterUser;
import com.rolland.twitter.service.user.Message;
import com.rolland.twitter.service.user.User;
import com.rolland.twitter.service.UserActions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
public class TwitterController {

    private final UserActions service;

    @Autowired
    public TwitterController(UserActions service) {
        this.service = service;
    }

    @PostMapping("lightweight-twitter/users")
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser(@RequestBody TwitterUser twitterUser) {
        service.registerUser(twitterUser);
    }

    @GetMapping("lightweight-twitter/users")
    @ResponseStatus(HttpStatus.OK)
    public List<User> getRegisteredUsers() {
        return service.getAllUsers();
    }

    @PostMapping("lightweight-twitter/users/{user}/messages")
    @ResponseStatus(HttpStatus.CREATED)
    public String postMessage(@PathVariable("user") String user, @RequestBody TwitterMessage twitterMessage) {
        return service.postMessage(user, twitterMessage.getText());
    }

    @PatchMapping("lightweight-twitter/follow/{follower}/{followed}")
    @ResponseStatus(HttpStatus.CREATED)
    public void followUser(@PathVariable("follower") String followerName, @PathVariable("followed") String followedName) {
        service.followUser(followerName, followedName);
    }

    @GetMapping("lightweight-twitter/unfollow/{follower}/{followed}")
    @ResponseStatus(HttpStatus.OK)
    public void unfollowUser(@PathVariable("follower") String followerName, @PathVariable("followed") String followedName) {
        service.unfollowUser(followerName, followedName);
    }

    @GetMapping("lightweight-twitter/users/{user}/follows")
    @ResponseStatus(HttpStatus.OK)
    public Set<User> followUser(@PathVariable("user") String userName) {
        return service.getFollowedUsers(userName);
    }

    @GetMapping("lightweight-twitter/users/{user}/messages")
    @ResponseStatus(HttpStatus.OK)
    public List<Message> getSubscriberMessages(@PathVariable("user") String userName) {
        return service.getTimelineMessages(userName);
    }
}
