package com.rolland.twitter.controller.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TwitterMessage {

    private String text;

    public TwitterMessage(String text) {
        this.text = text;
    }
}
