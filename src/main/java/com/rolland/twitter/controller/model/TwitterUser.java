package com.rolland.twitter.controller.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TwitterUser {
    private String name;

    public TwitterUser(String name) {
        this.name = name;
    }
}


