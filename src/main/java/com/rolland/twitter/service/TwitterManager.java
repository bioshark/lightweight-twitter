package com.rolland.twitter.service;


import com.rolland.twitter.service.user.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TwitterManager {

    private final Map<String, User> userRegistry = new HashMap<>();

    public void addUser(String userName) {
        if (!userRegistry.containsKey(userName)) {
            userRegistry.put(userName, User.of(userName));
        }
    }

    public User getUser(String userName) {
        if (userRegistry.containsKey(userName)) {
            return userRegistry.get(userName);
        }
        throw new IllegalArgumentException("User doesn't exist");
    }

    public List<User> getRegisteredUsers() {
        return new ArrayList<>(userRegistry.values());
    }

    public boolean isUserRegistered(String userName) {
        return userRegistry.containsKey(userName);
    }
}
