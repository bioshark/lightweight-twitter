package com.rolland.twitter.service;

import com.rolland.twitter.controller.model.TwitterUser;
import com.rolland.twitter.service.user.Message;
import com.rolland.twitter.service.user.User;

import java.util.List;
import java.util.Set;

public interface UserActions {
    String postMessage(String userName, String message);

    void registerUser(TwitterUser user);

    List<User> getAllUsers();

    void followUser(String followerName, String followedName);

    void unfollowUser(String followerName, String followedName);

    List<Message> getTimelineMessages(String userName);

    Set<User> getFollowedUsers(String userName);
}
