package com.rolland.twitter.service.user;

import com.rolland.twitter.controller.model.TwitterUser;
import com.rolland.twitter.service.TwitterManager;
import com.rolland.twitter.service.UserActions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class HandleUserActions implements UserActions {

    private final TwitterManager manager;

    @Autowired
    public HandleUserActions(TwitterManager mng) {
        this.manager = mng;
    }

    @Override
    public String postMessage(String userName, String message) {
        manager.getUser(userName).addMessage(message);
        return message;
    }

    @Override
    public void registerUser(TwitterUser user) {
        if (manager.isUserRegistered(user.getName())) {
            throw new IllegalArgumentException("User already registered");
        } else {
            manager.addUser(user.getName());
        }
    }

    @Override
    public List<User> getAllUsers() {
        return manager.getRegisteredUsers();
    }

    @Override
    public void followUser(String followerName, String followedName) {
        User follower = manager.getUser(followerName);
        User followed = manager.getUser(followedName);
        follower.followUser(followed);
    }

    @Override
    public void unfollowUser(String followerName, String followedName) {
        User follower = manager.getUser(followerName);
        User followed = manager.getUser(followedName);
        follower.unfollowUser(followed);
    }

    @Override
    public List<Message> getTimelineMessages(String userName) {
        User user = manager.getUser(userName);
        List<Message> followersMessages = new ArrayList<>(user.getMessages());

        for(User followed : user.getFollowedUsers()) {
            followersMessages.addAll(followed.getMessages());
        }

        Collections.sort(followersMessages);
        return followersMessages;
    }

    @Override
    public Set<User> getFollowedUsers(String userName) {
        return manager.getUser(userName).getFollowedUsers();
    }
}