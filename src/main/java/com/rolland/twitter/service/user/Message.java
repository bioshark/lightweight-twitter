package com.rolland.twitter.service.user;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@EqualsAndHashCode
public class Message implements Comparable<Message> {
    private final String content;
    private final LocalDateTime time;

    private Message(String message) {
        this.content = message;
        this.time = LocalDateTime.now();
    }

    public static Message of(String message) {
        return new Message(message);
    }

    @Override
    public int compareTo(Message o) {
        return this.getTime().isBefore(o.getTime()) ? -1 : 1;
    }
}
