package com.rolland.twitter.service.user;

import lombok.EqualsAndHashCode;

import java.util.*;

@EqualsAndHashCode
public class User {

    private final String name;
    private final List<Message> listOfMessage = new ArrayList<>();
    private final Set<User> followedUsers = new HashSet<>();

    private User(String name) {
        this.name = name;
    }

    public static User of(String name) {
        return new User(name);
    }

    public void addMessage(Message message) {
        listOfMessage.add(message);
    }

    public void addMessage(String value) {
        addMessage(Message.of(value));
    }

    public List<Message> getMessages() {
        return new ArrayList<>(listOfMessage);
    }

    public void followUser(User user) {
        this.followedUsers.add(user);
    }

    public void unfollowUser(User user) {
        this.followedUsers.remove(user);
    }

    public String getName() {
        return name;
    }

    public Set<User> getFollowedUsers() {
        return new HashSet<>(followedUsers);
    }
}
