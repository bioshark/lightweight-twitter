package com.rolland.twitter.service.user;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserTest {

    @Test
    void createUser() {
        User myself = User.of("Rolland");
        assertEquals("Rolland", myself.getName());
    }

    @Test
    void postMessage() {
        User myself = User.of("Rolland");
        Message m = Message.of("first message");
        myself.addMessage(m);

        assertEquals(1, myself.getMessages().size());
        assertEquals("first message", myself.getMessages().get(0).getContent());
    }

    @Test
    void followUnfollowUser() {
        User myself = User.of("Rolland");
        User bill = User.of("Bill");

        myself.followUser(bill);

        assertEquals(1, myself.getFollowedUsers().size());
        assertTrue(myself.getFollowedUsers().contains(bill));

        myself.unfollowUser(bill);
        assertEquals(0, myself.getFollowedUsers().size());

    }

    @Test
    void followMultipleUser() {
        User myself = User.of("Rolland");
        User bill = User.of("Bill");

        myself.followUser(bill);

        assertEquals(1, myself.getFollowedUsers().size());
        assertTrue(myself.getFollowedUsers().contains(bill));

        myself.followUser(bill);
        assertEquals(1, myself.getFollowedUsers().size());
    }
}